if exists("b:did_ftplugin")
        finish
endif
let b:did_ftplugin = 1

if version >= 703
    set colorcolumn=72
end
setlocal textwidth=72
